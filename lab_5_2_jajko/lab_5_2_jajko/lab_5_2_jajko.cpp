// lab2jajko.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include "pch.h"
#include <iostream>
#include <windows.h>
#include <gl/gl.h>
#include <gl/glut.h>
#include <ctime>


typedef float point3[3];

static GLfloat viewer[] = { 0.0, 0.0, 0.0 }; // inicjalizacja położenia obserwatora, viewer[2] przybliżanie
static GLfloat theta[] = { 0.0, 0.0, 0.0 }; // trzy kąty obrotu   // kąt obrotu obiektu

static GLfloat pix2angleX;     // przelicznik pikseli na stopnie
static GLfloat pix2angleY;
static GLfloat thetaY = 0.0;   // kąt obrotu obiektu

static GLint status = 0;       // stan klawiszy myszy 0 - nie naciśnięto żadnego klawisza, 1 - naciśnięty zostać lewy klawisz, 2 - naciśnięto prawy klawisz myszy

static int x_pos_old = 0;       // poprzednia pozycja kursora myszy
static int delta_x = 0;        // różnica pomiędzy pozycją bieżącą i poprzednią kursora myszy
static int y_pos_old = 0;
static int delta_y = 0;

const double MOJE_PI = 3.14159265359;
int model = 3;
const int N = 30;

double tab1[N][N][3]; 
double tabN[N][N][3];

int R = 20;
/*************************************************************************************/
// Mój program do rysowania jajka

struct point2
{
	float u;
	float v;
};
//struktura do pointa 3 wymiarowego
struct mPoint3
{
	float x;
	float y;
	float z;
};

struct clors
{
	float R;
	float G;
	float B;
};

clors colors[N][N];

//równania do krzywej Beziera
double x(double u, double v)
{
	return (-90.0 * u*u*u*u*u + 225 * u*u*u*u - 270 * u*u*u + 180 * u*u - 45 * u)*cos(MOJE_PI*v);
}

double y(double u, double v)
{
	return (160 * u*u*u*u - 320 * u*u*u + 160 * u*u) - 5;
}

double z(double u, double v)
{
	return (-90.0 * u*u*u*u*u + 225 * u*u*u*u - 270 * u*u*u + 180 * u*u - 45 * u)*sin(MOJE_PI*v);
}

//wektory normalne
double xu(double u, double v)
{
	return (-1 * 450 * u*u*u*u + 900 * u*u*u - 810 * u*u + 360 * u - 45)*cos(MOJE_PI*v);
}

double xv(double u, double v)
{
	return (MOJE_PI*(90 * u*u*u*u*u - 255 * u*u*u*u + 270 * u*u*u - 180 * u*u + 45 * u)*sin(MOJE_PI*v));
}

double yu(double u, double v)
{
	return (640 * u*u*u - 960 * u*u + 320 * u);
}

double yv(double u, double v)
{
	return 0;
}

double zu(double u, double v)
{
	return ((-450 * u*u*u*u + 900 * u*u*u - 810 * u*u + 360 * u - 45)*sin(MOJE_PI*v));
}

double zv(double u, double v)
{
	return (-1 * MOJE_PI*(90 * u*u*u*u*u - 225 * u*u*u*u + 270 * u*u*u - 180 * u*u + 45 * u)*cos(MOJE_PI*v));
}

mPoint3 normalizacja(double u, double v, int i)
{
	mPoint3 pN;
	pN.x = yu(u, v)*zv(u, v) - zu(u, v)*yv(u, v);
	pN.y = zu(u, v)*xv(u, v) - xu(u, v)*zv(u, v);
	pN.z = xu(u, v)*yv(u, v) - yu(u, v)*xv(u, v);

	double dlugosc = sqrt(pN.x*pN.x + pN.y*pN.y + pN.z*pN.z);
	if (i == N / 2)
	{
		pN.x /= 0;
		pN.y /= 1;
		pN.z /= 0;
	}
	else if (i == N || i == 0)
	{
		pN.x /= 0;
		pN.y /= -1;
		pN.z /= 0;
	}
	else if (i <= N / 2)
	{
		pN.x /= dlugosc;
		pN.y /= dlugosc;
		pN.z /= dlugosc;
	}
	else
	{
		pN.x /= -dlugosc;
		pN.y /= -dlugosc;
		pN.z /= -dlugosc;
	}

	return pN;
}

void Egg()
{
	float u, v;

	float xu, xv, yu, yv, zu, zv;

	point3* pointCloud = new point3[N * N];
	point3* normalVectors = new point3[N * N];

	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
		{
			u = i / (float)N;
			v = j / (float)N;

			pointCloud[i*N + j][0] = x(u, v);
			pointCloud[i*N + j][1] = y(u, v);
			pointCloud[i*N + j][2] = z(u, v);

			xu = (-450 * pow(u, 4) + 900 * pow(u, 3) - 810 * pow(u, 2) + 360 * u - 45) * cos(MOJE_PI * v);
			xv = MOJE_PI * (90 * pow(u, 5) - 225 * pow(u, 4) + 270 * pow(u, 3) - 180 * pow(u, 2) + 45 * u) * sin(MOJE_PI * v);
			yu = 640 * pow(u, 3) - 960 * pow(u, 2) + 320 * u;
			yv = 0;
			zu = (-450 * pow(u, 4) + 900 * pow(u, 3) - 810 * pow(u, 2) + 360 * u - 45) * sin(MOJE_PI * v);
			zv = -MOJE_PI * (90 * pow(u, 5) - 225 * pow(u, 4) + 270 * pow(u, 3) - 180 * pow(u, 2) + 45 * u) * cos(MOJE_PI * v);

			normalVectors[i*N + j][0] = yu * zv - zu * yv;
			normalVectors[i*N + j][1] = zu * xv - xu * zv;
			normalVectors[i*N + j][2] = xu * yv - yu * xv;

			float vectorLength = sqrt(pow(normalVectors[i*N + j][0], 2) + pow(normalVectors[i*N + j][1], 2) + pow(normalVectors[i*N + j][2], 2));

			normalVectors[i*N + j][0] /= vectorLength;
			normalVectors[i*N + j][1] /= vectorLength;
			normalVectors[i*N + j][2] /= vectorLength;

			if (i == N / 2)//góra jaja
			{
				normalVectors[i*N + j][0] *= 0;
				normalVectors[i*N + j][1] *= 1;
				normalVectors[i*N + j][2] *= 0;
			}
			else if (i == N || i == 0)//dół
			{
				normalVectors[i*N + j][0] *= 0;
				normalVectors[i*N + j][1] *= -1;
				normalVectors[i*N + j][2] *= 0;
			}
			else if (i <= N / 2)//przód
			{
				normalVectors[i*N + j][0] *= 1;
				normalVectors[i*N + j][1] *= 1;
				normalVectors[i*N + j][2] *= 1;
			}
			else //w tył
			{
				normalVectors[i*N + j][0] *= -1;
				normalVectors[i*N + j][1] *= -1;
				normalVectors[i*N + j][2] *= -1;
			}
		}

	glColor3f(1.0f, 1.0f, 1.0f); // Ustawienie koloru rysowania na bia�y

	if (model == 1)
	{
		glBegin(GL_POINTS);
		for (int j = 0; j < N; j++)
		{
			for (int i = 0; i < N; i++)
			{
				//glVertex3f(x(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j].v), y(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j].v), z(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j].v));
			}
		}
		glEnd();
	}
	else if (model == 2)
	{
		glBegin(GL_LINES);
		for (int j = 0; j < N; j++)
		{
			for (int i = 0; i < N; i++)
			{
/*
				if (i - 1 >= 0)
					glVertex3f(z(kwadratJednostkowy[i - 1][j].u, kwadratJednostkowy[i][j].v), y(kwadratJednostkowy[i - 1][j].u, kwadratJednostkowy[i][j].v), x(kwadratJednostkowy[i - 1][j].u, kwadratJednostkowy[i][j].v));
				else
					glVertex3f(z(kwadratJednostkowy[N - 1][j].u, kwadratJednostkowy[i][j].v), y(kwadratJednostkowy[N - 1][j].u, kwadratJednostkowy[i][j].v), x(kwadratJednostkowy[N - 1][j].u, kwadratJednostkowy[i][j].v));
				glVertex3f(z(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j].v), y(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j].v), x(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j].v));

				if (i - 1 >= 0)
					glVertex3f(z(kwadratJednostkowy[i - 1][j].u, kwadratJednostkowy[i][j].v), y(kwadratJednostkowy[i - 1][j].u, kwadratJednostkowy[i][j].v), x(kwadratJednostkowy[i - 1][j].u, kwadratJednostkowy[i][j].v));
				else
					glVertex3f(z(kwadratJednostkowy[N - 1][j].u, kwadratJednostkowy[i][j].v), y(kwadratJednostkowy[N - 1][j].u, kwadratJednostkowy[i][j].v), x(kwadratJednostkowy[N - 1][j].u, kwadratJednostkowy[i][j].v));
				if (j - 1 >= 0)
					glVertex3f(z(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j - 1].v), y(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j - 1].v), x(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j - 1].v));
				else
					glVertex3f(z(kwadratJednostkowy[i][N - 1].u, kwadratJednostkowy[i][j].v), y(kwadratJednostkowy[i][N - 1].u, kwadratJednostkowy[i][j - 1].v), x(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j].v));

				if (j - 1 >= 0)
					glVertex3f(z(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j - 1].v), y(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j - 1].v), x(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j - 1].v));
				else
					glVertex3f(z(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][0].v), y(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][0].v), x(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][0].v));
				glVertex3f(z(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j].v), y(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j].v), x(kwadratJednostkowy[i][j].u, kwadratJednostkowy[i][j].v));
			//*/
			}
		}
		glEnd();
	}
	else if (model == 3)
	{

		glBegin(GL_TRIANGLES);

		for (int i = 0; i < N - 1; i++)
		{
			for (int j = 0; j < N - 1; j++) 
			{
				glNormal3fv(normalVectors[j + i * N]);
				glVertex3fv(pointCloud[j + i * N]);

				glNormal3fv(normalVectors[j + (i + 1) * N]);
				glVertex3fv(pointCloud[j + (i + 1) * N]);

				glNormal3fv(normalVectors[j + 1 + (i + 1) * N]);
				glVertex3fv(pointCloud[j + 1 + (i + 1) * N]);

				glNormal3fv(normalVectors[j + 1 + (i + 1) * N]);
				glVertex3fv(pointCloud[j + 1 + (i + 1) * N]);

				glNormal3fv(normalVectors[j + i * N]);
				glVertex3fv(pointCloud[j + i * N]);

				glNormal3fv(normalVectors[j + 1 + i * N]);
				glVertex3fv(pointCloud[j + 1 + i * N]);
			}
		}

		for (int j = 0; j < N - 1; j++) 
		{

			glNormal3fv(normalVectors[j + (N - 1) * N]);
			glVertex3fv(pointCloud[j + (N - 1) * N]);

			glNormal3fv(normalVectors[j]);
			glVertex3fv(pointCloud[j]);

			glNormal3fv(normalVectors[j + 1]);
			glVertex3fv(pointCloud[j + 1]);


			glNormal3fv(normalVectors[j + 1]);
			glVertex3fv(pointCloud[j + 1]);


			glNormal3fv(normalVectors[j + (N - 1) * N]);
			glVertex3fv(pointCloud[j + (N - 1) * N]);

			glNormal3fv(normalVectors[j + 1 + (N - 1) * N]);
			glVertex3fv(pointCloud[j + 1 + (N - 1) * N]);
		}

		for (int i = 0; i < N - 2; i++) 
		{

			glNormal3fv(normalVectors[N - 1 + (i + 1) * N]);
			glVertex3fv(pointCloud[N - 1 + (i + 1) * N]); //19

			glNormal3fv(normalVectors[N - 1 + (i + 2) * N]);
			glVertex3fv(pointCloud[N - 1 + (i + 2) * N]); //29

			glNormal3fv(normalVectors[(N - 2 - i) * N]);
			glVertex3fv(pointCloud[(N - 2 - i) * N]); //80



			glNormal3fv(normalVectors[(N - 2 - i) * N]);
			glVertex3fv(pointCloud[(N - 2 - i) * N]); //80

			glNormal3fv(normalVectors[N - 1 + (i + 1) * N]);
			glVertex3fv(pointCloud[N - 1 + (i + 1) * N]); //19

			glNormal3fv(normalVectors[(N - 1 - i) * N]);
			glVertex3fv(pointCloud[(N - 1 - i) * N]); //90
		}

		glNormal3fv(normalVectors[N - 1]);
		glVertex3fv(pointCloud[N - 1]); //9

		glNormal3fv(normalVectors[N - 1 + N]);
		glVertex3fv(pointCloud[N - 1 + N]); //19

		glNormal3fv(normalVectors[(N - 1) * N]);
		glVertex3fv(pointCloud[(N - 1) * N]); //90


		glNormal3fv(normalVectors[(N - 1 + (N - 1) * N)]);
		glVertex3fv(pointCloud[(N - 1 + (N - 1) * N)]); //99

		glNormal3fv(normalVectors[N]);
		glVertex3fv(pointCloud[N]); //10

		glNormal3fv(normalVectors[0]);
		glVertex3fv(pointCloud[0]); //0


		/*glVertex3fv(pointCloud[0 + 1 * N]);
		glVertex3fv(pointCloud[0 + 2 * N]);
		glVertex3fv(pointCloud[1 + 1 * N]);

		glVertex3fv(pointCloud[1 + 2 * N]);
		glVertex3fv(pointCloud[0 + 2 * N]);
		glVertex3fv(pointCloud[1 + 1 * N]);*/

		glEnd();
	}

	delete[] pointCloud;
}

/*************************************************************************************/
// Funkcja rysująca osie układu współrzędnych
void Axes(void)
{

	point3  x_min = { -5.0, 0.0, 0.0 };
	point3  x_max = { 5.0, 0.0, 0.0 };
	// początek i koniec obrazu osi x

	point3  y_min = { 0.0, -5.0, 0.0 };
	point3  y_max = { 0.0,  5.0, 0.0 };
	// początek i koniec obrazu osi y

	point3  z_min = { 0.0, 0.0, -5.0 };
	point3  z_max = { 0.0, 0.0,  5.0 };
	//  początek i koniec obrazu osi y

	glColor3f(1.0f, 0.0f, 0.0f);  // kolor rysowania osi - czerwony
	glBegin(GL_LINES); // rysowanie osi x

	glVertex3fv(x_min);
	glVertex3fv(x_max);

	glEnd();

	glColor3f(0.0f, 1.0f, 0.0f);  // kolor rysowania - zielony
	glBegin(GL_LINES);  // rysowanie osi y

	glVertex3fv(y_min);
	glVertex3fv(y_max);

	glEnd();

	glColor3f(0.0f, 0.0f, 1.0f);  // kolor rysowania - niebieski
	glBegin(GL_LINES); // rysowanie osi z

	glVertex3fv(z_min);
	glVertex3fv(z_max);

	glEnd();

}

/*************************************************************************************/
// Funkcja określająca co ma być rysowane (zawsze wywoływana gdy trzeba
// przerysować scenę)
void RenderScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Czyszczenie okna aktualnym kolorem czyszczącym

	glLoadIdentity();
	// Czyszczenie macierzy bie??cej

	viewer[0] = R * cos(theta[0])*cos(thetaY);
	viewer[1] = R * sin(thetaY);
	viewer[2] = R * sin(theta[0])*cos(thetaY);

	if (cos(thetaY) > 0)
	{
		std::cout << "1\n";
		gluLookAt(viewer[0], viewer[1], viewer[2], 0.0, 0.0, 0.0, 0.0, 1.0, 0.0); //pierwsze 3 argumenty określają współrzędne obserwatora
																			  //kolejne 3 to punkt na który obserwator patrzy
																			  //ostatnie 3 to wektor skracania trzymanej kamery
	}
	// Zdefiniowanie położenia obserwatora
	else
	{
		std::cout << "-1\n";
		gluLookAt(viewer[0], viewer[1], viewer[2], 0.0, 0.0, 0.0, 0.0, -1.0, 0.0);
	}

	//Axes();
	// Narysowanie osi przy pomocy funkcji zdefiniowanej powyżej

	if (status == 1)                     // jeśli lewy klawisz myszy wcięnięty
	{
		theta[0] += delta_x * pix2angleX *0.05;   // modyfikacja kąta obrotu o kat proporcjonalny do różnicy położeń kursora myszy
		thetaY += delta_y * pix2angleY *0.05;
	}
	if (status == 2)                     // jeśli lewy klawisz myszy wcięnięty
	{
		R += delta_y;

		if (R > 26) //powyżej tego jajko oddalane znika
			R = 26;
		else if (R < 1) //jak blisko przybliżam
			R = 1;
	}

	if (thetaY == MOJE_PI)
	{
		thetaY = MOJE_PI + MOJE_PI / 2;
	}

	//if (status == 1)                     // je�li lewy klawisz myszy wci�ni�ty
	//{
	//	theta1 += delta1_x * pix2angleX / 180 * MOJE_PI;    // modyfikacja k�ta obrotu o kat proporcjonalny
	//	theta1_y += delta1_y * pix2angleX / 180 * MOJE_PI;
	//}

	//if (status == 2)                     // je�li lewy klawisz myszy wci�ni�ty
	//{
	//	theta2 += delta2_x * pix2angleX / 180 * MOJE_PI;    // modyfikacja k�ta obrotu o kat proporcjonalny
	//	theta2_y += delta2_y * pix2angleX / 180 * MOJE_PI;
	//}

	GLfloat up = 1.0;

	glRotatef(90, 0.0, 1.0, 0.0);
	glRotatef(thetaY, 1.0, 0.0, 0.0);

	Egg(); //rysowanie jajka

	glColor3f(1.0f, 1.0f, 1.0f);
	// Ustawienie koloru rysowania na biały

	//glutWireTeapot(3.0);
	// Narysowanie czajnika

	Axes();
	// Narysowanie osi przy pomocy funkcji zdefiniowanej wyżej

	glFlush();
	// Przekazanie poleceń rysujących do wykonania

	glutSwapBuffers();
	//

}
/*************************************************************************************/
// Funkcja ustalająca stan renderowania
void MyInit(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	// Kolor czyszcący (wypełnienia okna) ustawiono na czarny
/*************************************************************************************/
//  Definicja materiału z jakiego zrobiony jest czajnik
//  i definicja źródła światła
/*************************************************************************************/
/*************************************************************************************/
// Definicja materiału z jakiego zrobiony jest czajnik

	GLfloat mat_ambient[] = { 1.0, 1.0, 1.0, 1.0 };
	// współczynniki ka =[kar,kag,kab] dla światła otoczenia

	GLfloat mat_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	// współczynniki kd =[kdr,kdg,kdb] światła rozproszonego

	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	// współczynniki ks =[ksr,ksg,ksb] dla światła odbitego               

	GLfloat mat_shininess = { 20.0 };
	// współczynnik n opisujący połysk powierzchni
/*************************************************************************************/
// Definicja źródła światła
	GLfloat light_position[] = { 0.0, 0.0, 10.0, 1.0 };
	// położenie źródła

	GLfloat light_ambient[] = { 0.1, 0.1, 0.1, 1.0 };
	// składowe intensywności świecenia źródła światła otoczenia
	// Ia = [Iar,Iag,Iab]

	GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	// składowe intensywności świecenia źródła światła powodującego
	// odbicie dyfuzyjne Id = [Idr,Idg,Idb]

	GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	// składowe intensywności świecenia źródła światła powodującego
	// odbicie kierunkowe Is = [Isr,Isg,Isb]

	GLfloat att_constant = { 1.0 };
	// składowa stała ds dla modelu zmian oświetlenia w funkcji
	// odległości od źródła

	GLfloat att_linear = { 0.05 };
	// składowa liniowa dl dla modelu zmian oświetlenia w funkcji
	// odległości od źródła

	GLfloat att_quadratic = { 0.001 };
	// składowa kwadratowa dq dla modelu zmian oświetlenia w funkcji
	// odległości od źródła
/*************************************************************************************/
// Ustawienie parametrów materiału i źródła światła
/*************************************************************************************/
// Ustawienie patrametrów materiału
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);

	/*************************************************************************************/
	// Ustawienie parametrów źródła
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, att_constant);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, att_linear);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, att_quadratic);

	/*************************************************************************************/
	// Ustawienie opcji systemu oświetlania sceny
	glShadeModel(GL_SMOOTH); // właczenie łagodnego cieniowania
	glEnable(GL_LIGHTING);   // właczenie systemu oświetlenia sceny
	glEnable(GL_LIGHT0);     // włączenie źródła o numerze 0
	glEnable(GL_DEPTH_TEST); // włączenie mechanizmu z-bufora
/*************************************************************************************/
}
/*************************************************************************************/
// Funkcja ma za zadanie utrzymanie stałych proporcji rysowanych
// w przypadku zmiany rozmiarów okna.
// Parametry vertical i horizontal (wysokość i szerokość okna) są
// przekazywane do funkcji za każdym razem gdy zmieni się rozmiar okna.
void ChangeSize(GLsizei horizontal, GLsizei vertical)
{
	pix2angleX = 360.0 / (float)horizontal;  // przeliczenie pikseli na stopnie
	pix2angleY = 360.0 / (float)horizontal;

	glMatrixMode(GL_PROJECTION);
	// Przełączenie macierzy bieżącej na macierz projekcji

	glLoadIdentity();
	// Czyszcznie macierzy bieżącej

	gluPerspective(70, 1.0, 1.0, 30.0);
	// Ustawienie parametrów dla rzutu perspektywicznego

	if (horizontal <= vertical)
		glViewport(0, (vertical - horizontal) / 2, horizontal, horizontal);

	else
		glViewport((horizontal - vertical) / 2, 0, vertical, vertical);
	// Ustawienie wielkości okna okna widoku (viewport) w zależności
	// relacji pomiędzy wysokością i szerokością okna

	glMatrixMode(GL_MODELVIEW);
	// Przełączenie macierzy bieżącej na macierz widoku modelu  

	glLoadIdentity();
	// Czyszczenie macierzy bieżącej

}

void spinEgg()
{
	theta[0] -= 0.09;
	if (theta[0] > 360.0) theta[0] -= 360.0;

	theta[1] -= 0.09;
	if (theta[1] > 360.0) theta[1] -= 360.0;

	theta[2] -= 0.09;
	if (theta[2] > 360.0) theta[2] -= 360.0;

	glutPostRedisplay(); //odświeżenie zawartości aktualnego okna
}

void keys(unsigned char key, int x, int y)
{
	if (key == 'p')
	{
		std::cout << "p\n";
		model = 1;
	}
	if (key == 'w')
	{
		std::cout << "w\n";
		model = 2;
	}
	if (key == 's')
	{
		std::cout << "s\n";
		model = 3;
	}
	RenderScene(); // przerysowanie obrazu sceny
}

void Mouse(int btn, int state, int x, int y)
{
	if (btn == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		x_pos_old = x;      // przypisanie aktualnie odczytanej pozycji kursora jako pozycji poprzedniej
		y_pos_old = y;
		status = 1;         // wcięnięty został lewy klawisz myszy
	}
	else if (btn == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		x_pos_old = x;
		y_pos_old = y;
		status = 2;         //wciśnięty został prawy klawisz myszy
	}
	else
		status = 0;         // nie został wcięnięty żaden klawisz
}

void Motion(GLsizei x, GLsizei y)
{
	delta_x = x - x_pos_old;     // obliczenie różnicy położenia kursora myszy
	x_pos_old = x;            // podstawienie bieżącego położenia jako poprzednie

	delta_y = y - y_pos_old;
	y_pos_old = y;

	glutPostRedisplay();     // przerysowanie obrazu sceny
}

/*************************************************************************************/
// Główny punkt wejścia programu. Program działa w trybie konsoli
void main(int argc, char* argv[])
{
	srand(time(NULL));
	for (int j = 0; j < N; j++)
	{
		for (int i = 0; i < N; i++)
		{
			colors[i][j].R = 34.0f;
			colors[i][j].G = 139.0f;
			colors[i][j].B = 34.0f;
		}
	}

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(300, 300);

	glutInit(&argc, argv);
	glutCreateWindow("Obracanie jajka");
	glutKeyboardFunc(keys);
	//glutIdleFunc(spinEgg);

	glutMouseFunc(Mouse);
	// Ustala funkcję zwrotną odpowiedzialną za badanie stanu myszy

	glutMotionFunc(Motion);
	// Ustala funkcję zwrotną odpowiedzialną za badanie ruchu myszy

	glutDisplayFunc(RenderScene);
	// Określenie, że funkcja RenderScene będzie funkcją zwrotną
	// (callback function).  Bedzie ona wywoływana za każdym razem
	// gdy zajdzie potrzba przeryswania okna

	glutReshapeFunc(ChangeSize);
	// Dla aktualnego okna ustala funkcję zwrotną odpowiedzialną
	// zazmiany rozmiaru okna      

	MyInit();
	// Funkcja MyInit() (zdefiniowana powyżej) wykonuje wszelkie
	// inicjalizacje konieczne  przed przystąpieniem do renderowania

	glEnable(GL_DEPTH_TEST);
	// Włączenie mechanizmu usuwania powierzchni niewidocznych

	glutMainLoop();
	// Funkcja uruchamia szkielet biblioteki GLUT
}
/*************************************************************************************/